package Sistemas;

import Sistemas.Clases.Cliente;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Scanner;




public class RegistroCliente extends JDialog {
    private JPanel contentPane;
    private JTextField Nombretxt;
    private JTable table1;
    private JTextField Direcciontxt;
    private JTextField Telefonotxt;
    private JButton registrarButton;
    private JButton salirButton;
    private JTextField Dpitxt;
    private JButton buttonOK;
    private String dpi;
    private String nombre;
    private String direccion;
    private String telefono;


    void Limpia (){
        Dpitxt.setText("");
        Nombretxt.setText("");
        Direcciontxt.setText("");
        Telefonotxt.setText("");
    }

    ArrayList<Cliente> lista = new ArrayList<>();

    public void mostrar()
    {
        String Matris [][] = new String[lista.size()][4];
        for (int i = 0; i < lista.size(); i++)
        {
            Matris[i][0]=lista.get(i).getDpi();
            Matris[i][1]=lista.get(i).getNombre();
            Matris[i][2]=lista.get(i).getDireccion();
            Matris[i][3]=lista.get(i).getTelefono();
        }

        table1.setModel(new javax.swing.table.DefaultTableModel(
                Matris,
                new String []{"DPI","Nombre","Direccion","Telefono"}
        ));

    }



    public RegistroCliente() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setSize(500,500);
        setLocationRelativeTo(null);
        dispose();
        Scanner scan = new Scanner(System.in);

        salirButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        registrarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dpi = Dpitxt.getText(); nombre = Nombretxt.getText();  direccion = Direcciontxt.getText(); telefono = Telefonotxt.getText();
                Cliente clien = new Cliente(dpi,nombre,direccion,telefono);
                lista.add(clien);
                mostrar();
                Limpia();


            }
        });
    }
}
