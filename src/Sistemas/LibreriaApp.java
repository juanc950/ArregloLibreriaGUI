package Sistemas;

import Sistemas.Clases.Cliente;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class LibreriaApp extends JDialog {
    private JPanel contentPane;
    private JTextPane sistemaQueRegistraElTextPane;
    private JButton registroClienteButton;
    private JButton registroLibrosButton;
    private JButton registroEmpleadoButton;
    private JButton libreriaButton;
    private JButton buttonOK;

    public LibreriaApp() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setLocation(500,200);
        setSize(500,500);

        registroClienteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RegistroCliente regClien = new RegistroCliente();
                regClien.setVisible(true);
                regClien.setLocationRelativeTo(null);
            }
        });
        registroLibrosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RegistroLibros regLibro = new RegistroLibros();
                regLibro.setVisible(true);
                regLibro.setLocationRelativeTo(null);
            }
        });
        registroEmpleadoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RegistroEmpleados regEmpl = new RegistroEmpleados();
                regEmpl.setVisible(true);
                regEmpl.setLocationRelativeTo(null);
            }
        });
    }

    public static void main(String[] args) {
        LibreriaApp dialog = new LibreriaApp();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);


    }
}
