package Sistemas.Clases;

/**
 * Created by juancarlos on 13/07/2017.
 */
public class Empleado {

    private String Dpi;
    private String Nombre;
    private String Direccion;
    private String Telefono;

    public Empleado() {
    }

    public Empleado(String dpi, String nombre, String direccion, String telefono) {
        Dpi = dpi;
        Nombre = nombre;
        Direccion = direccion;
        Telefono = telefono;
    }

    public String getDpi() {
        return Dpi;
    }

    public void setDpi(String dpi) {
        Dpi = dpi;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }
}
