package Sistemas.Clases;

/**
 * Created by juancarlos on 13/07/2017.
 */
public class Libros {

    private String Nombre;
    private String Titulo;
    private String Categoria;
    private String Editorial;
    private String NumPagina;
    private String Precio;

    public Libros() {
    }

    public Libros(String nombre, String titulo, String categoria, String editorial, String numPagina, String precio) {
        Nombre = nombre;
        Titulo = titulo;
        Categoria = categoria;
        Editorial = editorial;
        NumPagina = numPagina;
        Precio = precio;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String titulo) {
        Titulo = titulo;
    }

    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String categoria) {
        Categoria = categoria;
    }

    public String getEditorial() {
        return Editorial;
    }

    public void setEditorial(String editorial) {
        Editorial = editorial;
    }

    public String getNumPagina() {
        return NumPagina;
    }

    public void setNumPagina(String numPagina) {
        NumPagina = numPagina;
    }

    public String getPrecio() {
        return Precio;
    }

    public void setPrecio(String precio) {
        Precio = precio;
    }
}
